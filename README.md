<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">openMAXIMS - openMAXIMS PAS/EPR 的主页</font></font></h1><a id="user-content-openmaxims---home-of-the-openmaxims-pasepr" class="anchor" aria-label="永久链接：openMAXIMS - openMAXIMS PAS/EPR 的主页" href="#openmaxims---home-of-the-openmaxims-pasepr"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<hr>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">##临床安全</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">IMS MAXIMS 绝对不保证该软件程序的临床安全性。该软件的用户完全自行承担风险。IMS MAXIMS 仅确保其构建、部署和维护的未更改的运行时软件的临床安全性。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果发现任何临床安全问题，IMS MAXIMS 将发布临床安全通知。因此，您在使用此源代码时需要将您的联系方式发送至</font></font><a href="mailto:openmaxims-user@imsmaxims.com"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">openmaxims-user@imsmaxims.com</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，以便您能够收到临床安全通知。</font></font></p>
<hr>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">openMAXIMS 功能</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们全套 MAXIMS 套件的最新一代产品已成为 openMAXIMS，这是我们的第一款开源软件产品。它具有相同的丰富功能，目前包括：</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为 NHS 设计的完整患者管理系统，包括对以下方面的支持：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NHS 电子转诊服务（获得 HSCIC 的全面推出批准）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">调试数据集</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">转诊至治疗管理 (RTT)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图形化床位管理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">门诊预约管理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CQUIN目标</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">电子病历包括：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">订单沟通和结果报告</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">综合护理途径</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">观察和注释</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">急诊科（A&amp;E）包括追踪</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">临床评估</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">临床专科（如肿瘤学、脊髓损伤等）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">eDischarge 流程</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HL7 接口（双向）和开放 API，可与第三方系统全面集成</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">openMAXIMS 版本 10.5</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是 Taunton and Somerset NHS Trust 上线的版本。它增加了许多新功能，包括：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">临床转诊分类——根据临床优先级将患者引导至适当的服务和护理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">选修列表管理 - 住院候补名单和 TCI 管理与电子转诊服务、RTT 管理和企业范围的调度集成</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">手术室 - 术前评估 - 确保患者身体健康并适合接受手术</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">预约结果 - 根据预约状态和 RTT 制定的工作流程和行动</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">临床编码 - 基于各种编码状态点的工作列表 - 进行中、待审核、已编码、未编码等</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">...以及对病例跟踪、等候名单、调度、住院患者、TCI、自动取消、SOAP、出院、剧院、治疗、病房视图、死者、人口统计、工作列表、急诊科、门诊、医疗工作列表、合同、RTT、选修课列表、床位管理、配置、预评估、痴呆症、HL7 消息传递、病房转移、转诊、安全 LDAP、观察、救护车交接等的许多其他增强功能。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们种类繁多的产品均已完全集成，并且是与我们的 NHS 客户合作从头开始构建的。这意味着医疗保健提供商现在可以免费获得多年的专业知识和投资。IMS MAXIMS 强烈鼓励最终用户组织接受我们的支持合同，没有比我们更好的人可以为 openMAXIMS 产品套件提供支持、实施、培训、开发、交付变更管理、项目管理和计划管理服务。</font></font></p>
</article></div>
